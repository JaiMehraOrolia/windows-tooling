from typing import Optional, List
from tooling.protocol.snmp import SNMPClient
import re


class PDU8100X(SNMPClient):
    ''' Interface class for controlling the Cyberpower PDU8100X family of
        devices via SNMP.

        Notes:
         - Datasheet can be found in the downloads section of the following
           page:
           https://www.cyberpowersystems.com/product/pdu/switched-mbo/pdu81006/
         - Only works on linux (as this package uses the 'snmpwalk' bash
           command)
         - Ensure the 'snmp' system package is installed
    '''

    OID_base = "1.3.6.1.4.1.3808"

    # OID to get number of total outlets in the PDU
    ePDUOutletDevNumTotalOutlets_OID = "1.1.3.3.1.4"

    # OID to control all outlets simultaneously
    ePDUOutletDevCommand_OID = "1.1.3.3.1.1.0"
    # Valid commands for controlling all outlets simultaneously
    ePDUOutletDevCommand_VALUES = {
        "on": 2,
        "off": 3,
        "reboot": 4,
    }
    ePDUOutletDevCommand_VALUES_REVERSE = {
        2: "on",
        3: "off",
        4: "reboot"
    }

    # OID to control individual outlets
    ePDUOutletControlCommand_OID = "1.1.3.3.3.1.1.4"
    # Valid commands for controlling each outlets individually
    ePDUOutletControlCommand_VALUES = {
        "on": 1,
        "off": 2,
        "reboot": 3
    }
    ePDUOutletControlCommand_VALUES_REVERSE = {
        1: "on",
        2: "off",
        3: "reboot"
    }

    # OID to get state of individual outlets
    ePDUOutletStatusOutletState_OID = "1.1.3.3.5.1.1.4"
    # Dictionary that maps returned outlet status to a human readable form
    ePDUOutletStatusOutletState_RESULTS = {
        1: "on",
        2: "off"
    }

    # OID to control outlet names
    ePDUOutletControlOutletName_OID = "1.1.3.3.3.1.1.2"

    def __init__(self, ip: str, version: str = '1') -> None:
        """Constructor for the PDU8100X class

        Keyword arguments:
        ip -- IPv4 address of the Linkbone device
        version -- SNMP version to use ('1' or '2c')
        """
        if version not in ('1', '2c'):
            raise ValueError("PDU8100X SNMP version must be either 1 or 2c "
                             "(representing v1 & v2c protocols)")
        super().__init__(ip=ip, version=version)

    def get_total_outlets(self) -> Optional[int]:
        """Gets the total number of controllable outputs

        Returns:
        An integer representing the total number of controllable
        outlets, or None on error
        """
        oid = f"{self.OID_base}.{self.ePDUOutletDevNumTotalOutlets_OID}"
        result = self.snmpwalk(oid=oid)
        if result is None:
            return None

        return result[0].value

    def get_outlet_status(self, outlet: int) -> Optional[str]:
        """Gets the active state of a controllable outlet

        Keyword arguments:
        outlet -- The outlet number

        Returns:
        A string representing the active state of the outlet,
        or None on error
        """
        if type(outlet) != int or outlet <= 0:
            raise ValueError("outlet must be a positive integer")

        o = f"{self.OID_base}.{self.ePDUOutletStatusOutletState_OID}.{outlet}"
        result = self.snmpget(oid=o)
        if result is None:
            return None

        return self.ePDUOutletStatusOutletState_RESULTS[result[0].value]

    def get_all_outlet_statuses(self) -> List[str]:
        """Gets the active state of all controllable outlets

        Returns:
        A list of strings representing the active state of all the
        outlets, or None on error
        """
        oid = f"{self.OID_base}.{self.ePDUOutletStatusOutletState_OID}"
        result = self.snmpwalk(oid=oid)
        return [
            self.ePDUOutletStatusOutletState_RESULTS[value.value]
            for value in result
        ]

    def toggle_outlet(self, outlet: int, state: str) -> Optional[str]:
        """Sets the active state of a controllable outlet

        Keyword arguments:
        outlet -- The outlet number
        state -- The active state to set the outlet to
                 (on, off, reboot)

        Returns:
        A string representing the new active state of the
        outlet, or None on error
        """
        if type(outlet) != int or outlet <= 0:
            raise ValueError("outlet must be a positive integer")
        values = self.ePDUOutletControlCommand_VALUES
        if type(state) != str or state not in values:
            raise ValueError(f"state must be one of {values.keys()}")

        oid = f"{self.OID_base}.{self.ePDUOutletControlCommand_OID}.{outlet}"
        value_type = "i"
        result = self.snmpset(oid=oid, value_type=value_type,
                              value=values[state])
        if result is None:
            return None

        return self.ePDUOutletControlCommand_VALUES_REVERSE[result[0].value]

    def toggle_all_outlets(self, state: str) -> Optional[str]:
        """Sets the active state of a controllable outlet

        Keyword arguments:
        state -- The active state to set the outlets to
                 (on, off, reboot)

        Returns:
        A string representing the new active state of all
        the outlets, or None on error
        """
        if type(state) != str or state not in self.ePDUOutletDevCommand_VALUES:
            raise ValueError("state must be one of "
                             f"{self.ePDUOutletDevCommand_VALUES.keys()}")

        oid = f"{self.OID_base}.{self.ePDUOutletDevCommand_OID}"
        value_type = "i"
        result = self.snmpset(
            oid=oid,
            value_type=value_type,
            value=self.ePDUOutletDevCommand_VALUES[state]
        )

        if result is None:
            return None

        return self.ePDUOutletDevCommand_VALUES_REVERSE[result[0].value]

    def get_name(self, outlet: int) -> str:
        """Gets the name of an outlet

        Keyword arguments:
        outlet - The outlet number

        Returns:
        A string containing the name of the outlet
        """
        o = f"{self.OID_base}.{self.ePDUOutletControlOutletName_OID}.{outlet}"
        result = self.snmpget(o)

        return result.value[0].decode()

if __name__ == "__main__":
    pdu = PDU8100X(ip="10.10.179.149")
