from typing import Any, List, Optional
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto.rfc1905 import EndOfMibView
from collections import namedtuple

class SNMPClient:
    OidReturn = namedtuple("OidReturn", ["oid", "value"])

    # Valid snmpset value types
    snmpset_value_types = {
        'i': cmdgen.Integer,
        'u': cmdgen.Unsigned32,
        't': cmdgen.TimeTicks,
        'a': cmdgen.IpAddress,
        'o': cmdgen.ObjectIdentifier,
        's': cmdgen.OctetString,
        'x': cmdgen.OctetString,
        'd': cmdgen.OctetString,
        'b': cmdgen.OctetString,
    }
    snmp_versions = ('1', '2c', '3')

    def __init__(self, ip: str, version: str, *, timeout: int = 10) -> None:
        if version not in self.snmp_versions:
            raise ValueError("SNMP version must be one of '1', '2c', or '3'")
        if version == '3':  #TODO: Add SNMPv3 support:
            raise NotImplementedError("SNMPv3 support is not yet implemented for this class.")

        self.ip = ip
        self.version = version
        self.timeout = timeout
        self.community = "private"  #TODO: Make dynamic

        self.snmp = cmdgen.CommandGenerator()

    def set_ip(self, ip: int) -> None:
        """Sets the IP address to target SNMP requests to

        Keyword arguments:
        ip -- The address of the target device
        """
        if type(ip) != str:
            raise ValueError("ip must a string in standard ipv4 format '192.168.1.100'")
        self.ip = ip

    def set_version(self, version: int) -> None:
        """Sets the SNMP version of the requests

        Keyword arguments:
        version -- SNMP version of the request ('1', '2c', or '3')
        """
        if version not in self.snmp_versions:
            raise ValueError("version must be either '1', '2c', or '3'")
        self.version = version
    
    def set_timeout(self, timeout: int) -> None:
        """Sets the SNMP query timeout
        
        Keyword arguments:
        timeout -- The timeout in seconds
        """
        self.timeout = timeout

    def _validate_snmpset_value(self, value_type: str, value: Any) -> ...:
        if value_type not in self.snmpset_value_types:
            raise ValueError(f"Valid snmpset value types are {self.snmpset_value_types}")

        return self.snmpset_value_types[value_type](value)

    def snmpwalk(self, oid: str) -> Optional[List[OidReturn]]:
        """snmpwalk query to the device

        Keyword arguments:
        oid -- The SNMP object identifier to query

        Returns:
        A string representing the output of the snmpwalk query or None
        if an error occured
        """
        errorIndication, _, _ , varBinds = self.snmp.nextCmd(
            cmdgen.CommunityData(self.community),
            cmdgen.UdpTransportTarget((self.ip, 161), timeout=60),
            cmdgen.MibVariable(oid),
            lookupMib=False
        )

        if errorIndication:
            return None

        parsed_values = [
            self.OidReturn(str(oid), val._value) 
            for varBind in varBinds for (oid,val) in varBind 
            if type(val) is not EndOfMibView
        ]
        
        return parsed_values

    def snmpset(self, oid: str, value_type: str, value: Any) -> Optional[str]:
        """snmpset query to the device

        Keyword arguments:
        oid -- The SNMP object identifier to query
        value_type -- The type of the value being written to the device
                      (e.g. integer, string, etc..)
        value -- The value being written to the device

        Returns:
        A string representing the output of the snmpset query or None
        if an error occured
        """
        value = self._validate_snmpset_value(value_type=value_type, value=value)
        
        errorIndication, _, _ , varBinds = self.snmp.setCmd(
            cmdgen.CommunityData(self.community),
            cmdgen.UdpTransportTarget((self.ip, 161), timeout=60),
            cmdgen.ObjectType(cmdgen.MibVariable(oid), value),
            lookupMib=False,
        )

        if errorIndication:
            return None

        parsed_values = [
            self.OidReturn(str(oid), val._value) 
            for (oid,val) in varBinds
            if type(val) is not EndOfMibView
        ]

        return parsed_values

    def snmpget(self, *oids: str) -> Optional[str]:
        """snmpget query to the device

        Arguments:
        oid -- The SNMP object identifier(s) to query

        Returns:
        A string representing the output of the snmpget query or None
        if an error occured
        """
        errorIndication, _, _ , varBinds = self.snmp.getCmd(
            cmdgen.CommunityData(self.community),
            cmdgen.UdpTransportTarget((self.ip, 161), timeout=60),
            *[cmdgen.MibVariable(oid) for oid in oids],
            lookupMib=False,
        )

        if errorIndication:
            return None

        parsed_values = [
            self.OidReturn(str(oid), val._value) 
            for (oid,val) in varBinds
            if type(val) is not EndOfMibView
        ]

        return parsed_values

if __name__ == "__main__":
    import sys
    ip = sys.argv[1]
    snmp = SNMPClient(ip, version="1")